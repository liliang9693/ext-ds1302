# DS1302 实时时钟模块


![](./arduinoC/_images/featured.png)

---------------------------------------------------------

## 目录

* [相关链接](#相关链接)
* [描述](#描述)
* [积木列表](#积木列表)
* [示例程序](#示例程序)
* [许可证](#许可证)
* [支持列表](#支持列表)
* [更新记录](#更新记录)

## 相关链接
* 本项目加载链接: ```https://gitee.com/liliang9693/ext-ds1302```

* 用户库教程链接: ```https://mindplus.dfrobot.com.cn/extensions-user```



## 描述
可以从Mind+中加载的DS1302扩展库，供测试，欢迎提PR

## 积木列表

![](./arduinoC/_images/blocks.png)



## 示例程序

![](./arduinoC/_images/example1.png)
![](./arduinoC/_images/example2.png)

## 许可证

MIT

## 支持列表

主板型号                | 实时模式    | ArduinoC   | MicroPython    | 备注
------------------ | :----------: | :----------: | :---------: | -----
micro:bit        |             |       x       |             | 
mpython        |             |        ×      |             | 
arduino        |             |        √      |             | 

## 鸣谢
感谢Mind+官网QQ群中的**特工李**测试

## 更新日志
* V0.0.1  基础功能完成,未经过实物测试
* V0.0.2  增加其他arduino主控板的支持，arduino uno经过实物测试
* V0.0.3  修复时间为单数时时间错误的问题
* V0.0.4  调整include位置，修复新版本报错问题