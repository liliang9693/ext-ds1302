
enum SIZE {
    //% block="29*29"
    1,
    //% block="58*58"
    2
}

enum LINE {
    //% block="1"
    1,
    //% block="2"
    2,
    //% block="3"
    3,
    //% block="4"
    4
}

enum BTN {
    //% block="A"
    A,
    //% block="B"
    B,
    //% block="A+B"
    AB
}
var monlist:string[] = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];//日期转换

//% color="#a58c5b" iconWidth=50 iconHeight=40
namespace ds1302 {
    //% block="DS1302 initialization pin RST[RST] DAT[DAT] CLK[CLK]" blockType="command"
    //% RST.shadow="dropdown" RST.options="PIN_DigitalWrite" 
    //% DAT.shadow="dropdown" DAT.options="PIN_DigitalRead" 
    //% CLK.shadow="dropdown" CLK.options="PIN_DigitalWrite" 

    export function ds1302Init(parameter: any, block: any) {
        let rst = parameter.RST.code;
        let dat = parameter.DAT.code;
        let clk = parameter.CLK.code;
        
        Generator.addInclude("include1302",`#include <ThreeWire.h>\n#include <RtcDS1302.h>`);

        Generator.addObject("wireobject","ThreeWire","myWire(3,4,2);");
        Generator.addObject("1302object","RtcDS1302<ThreeWire>","Rtc(myWire);");
        Generator.addSetup("1302setup","Rtc.Begin();")

    }

    //% block="DS1302 get time[TIME]" blockType="reporter"
    //% TIME.shadow="dropdown" TIME.options="TIME" 
    export function ds1302Get(parameter: any, block: any) {
        let time = parameter.TIME.code

        Generator.addCode([`Rtc.GetDateTime().${time}()`,Generator.ORDER_UNARY_POSTFIX]);
    }

     //% block="---"
     export function noteSep() {

    }

    //% block="DS1302 calibration time M:[MOM] D:[DAY] Y:[YEAR]  H:[HOUR] M:[MIN] S:[SEC] " blockType="command"
    //% YEAR.shadow="number" YEAR.defl='2020'
    //% MOM.shadow="number" MOM.defl='1'
    //% DAY.shadow="number" DAY.defl='1'
    //% HOUR.shadow="number" HOUR.defl='8'
    //% MIN.shadow="number" MIN.defl='0'
    //% SEC.shadow="number" SEC.defl='0'
    export function ds1302Reset(parameter: any, block: any) {
        let year = parameter.YEAR.code
        let mon = parameter.MOM.code
        let day = parameter.DAY.code

        let hour = parameter.HOUR.code
        let min = parameter.MIN.code
        let sec = parameter.SEC.code

        mon=monlist[mon-1];//转换为英文代号

        if(day.length == 1){//单日期加0
            day = '0'+day;
        }

        if(hour.length == 1){//单时加0
            hour = '0'+hour;
        }
        if(min.length == 1){//单分加0
            min = '0'+min;
        }
        if(sec.length == 1){//单秒加0
            sec = '0'+sec;
        }
        

         Generator.addSetup(`Rtc.SetDateTime`, `Rtc.SetDateTime(RtcDateTime("${mon}/${day}/${year}", "${hour}:${min}:${sec}"));`);

    }

}
